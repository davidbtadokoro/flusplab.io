---
title:  "Happy Hour"
image:  "happy_hour"
ref: "happy_hour"
categories: "patrocinio.md"
id: 4
---

# Valor: R$250

# Benefícios

No FLUSP nos valorizamos muito as interações sociais, por isso, ao final do
evento teremos um happy hour com os participantes. Nesse sentido, oferecemos um
espaço extra de divulgação durante o happy hour do evento na forma de banners
ou distribuição de panfletos.
