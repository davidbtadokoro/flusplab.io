---
title: "Achievements"
---

## Overview

In the last years, FLUSP members have been dedicating time and effort to
contribute with FLOSS projects. In this section, we describe for a set of FLOSS
projects what we achieved so far:

### Linux Kernel:

{% include add_image.html
   absolute="true"
   src="/img/achievements/kernel_contributions.png"
   caption="Source: https://www.spinics.net/lists/linux-iio/msg41629.html" %}

* The group accounted for 20% of all contributions made to the IIO subsystem
in the period from 10/03/2018 to 11/17/2018.

{% include add_image.html
   absolute="true"
   src="/img/achievements/kernel_contributions_new.jpg"
   style="width: 33%; height: auto;"
   caption="Source: https://www.spinics.net/lists/linux-iio/msg43827.html" %}

* From 01/04/2019 to 04/04/2019, the percentage of contributions made by FLUSP
  members to the IIO subsystem was 19.4%, almost the same percentage of 2018.
* There are approximately 115 patches made by FLUSP members in the main tree of
  the Linux Kernel.
* Three drivers containing FLUSP contributions were moved from staging one
  driver in the DRM subsystem.
* One of our members is the maintainer of the VKMS, which is a driver in the
  DRM subsystem.
* Due to FLUSP’s efforts in contributing to drivers in the IIO subsystem, a
  company donated more them four hardware boards. An explanation of each one of
  the boards together with useful links (such as the datasheets) can be found
  at the [resources]({{ site.baseurl }}{% link resources.md %}).

### Git:

* One FLUSP member is working in the Git version-control system. This member
  submitted seven patches to the official repository.
* The FLUSP member who contributes with Git also applied for GSoC'2019. The
  project proposal is to make the Git codebase more thread-safe, such
  that in the future it will be possible to improve Git's parallelism.
* The FLUSP members also gave classes on Git to the freshmen of the Institute
  of Mathematics and Statistics (IME-USP).

### GCC:

* There is one FLUSP member actively working on the GNU Compiler Collection
  (GCC). This member has already three patches accepted, and 11 submissions
  made. He also applied to GSoC'19 with a GCC project related to parallelize
  part of the compilation.
